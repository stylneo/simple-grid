from grid import Grid
import tkinter as tk

world = None
grid = None

def countNeighbors(world, x, y):
    assert 0 <= x < len(world[0])
    assert 0 <= y < len(world)

    def inBounds(x, y, max_x, max_y):
        return (0 <= x <= max_x) and (0 <= y <= max_y)

    neighbors = 0
    for dx in (-1, 0, 1):
        for dy in (-1, 0, 1):
            if (dx == 0 and dy == 0) or not inBounds(
                x + dx, y + dy, len(world[0]) - 1, len(world) - 1
            ):
                continue
            if world[y+dy][x+dx] == 1:
                neighbors += 1
    return neighbors


def processWorld(world):
    newWorld = [[0 for x in range(len(world[0]))] for y in range(len(world))]

    for y in range(len(world)):
        for x in range(len(world[y])):
            n_neighbors = countNeighbors(world, x, y)
            if world[y][x] == 1:
                if 2 <= n_neighbors <= 3:
                    newWorld[y][x] = 1
            else:
                if n_neighbors == 3:
                    newWorld[y][x] = 1
    return newWorld

def paintWorld(grid: Grid, world: list[list[int]]):
        grid.setAll(world)

def run():
    global world, grid
    paintWorld(grid, world)
    world = processWorld(world)
    grid.after(500, lambda: run())

def main():
    global world, grid
    world = [[0 for x in range(40)] for y in range(40)]
 
    world[0][1] = 1
    world[2][0] = 1
    world[2][1] = 1
    world[2][2] = 1
    world[1][2] = 1

    window = tk.Tk()
    grid = Grid(master=window)
    run()
    window.mainloop()

if __name__ == "__main__":
    main()
