import tkinter as tk


class Grid(tk.Frame):
	def __init__(
		self,
		master: tk.Tk = None,
		window_size: tuple[int, int] = (800, 800),
		pixel_size: tuple[int, int] = (20, 20),
		*args,
		**kwargs,
	) -> None:
		super().__init__(master, *args, **kwargs)

		self.width = window_size[0]
		self.height = window_size[1]
		self.pixel_w = pixel_size[0]
		self.pixel_h = pixel_size[1]

		assert self.width % self.pixel_w == 0
		assert self.height % self.pixel_h == 0

		max_x = self.width // self.pixel_w
		max_y = self.height // self.pixel_h

		self.canvas = tk.Canvas(master=self)

		# The ids of each cell
		self.cells = [
			[
				self.canvas.create_rectangle(
					x * self.pixel_w,
					y * self.pixel_h,
					(x + 1) * self.pixel_w,
					(y + 1) * self.pixel_h,
					fill="white",
				)
				for x in range(self.width // self.pixel_w)
			]
			for y in range(self.height // self.pixel_h)
		]

		self.__setup()
		self.__setupEvents()
		self.__drawGridLines()
		self.updateGrid()

	def __setup(self) -> None:
		self.master.geometry(f"{self.width}x{self.height}")
		self.master.resizable(height=False, width=False)

		self.pack(expand=True, fill="both")
		self.canvas.pack(expand=True, fill="both")
		self.canvas.configure(bg="white")

	def __setupEvents(self) -> None:
		self.canvas.bind(
			"<B1-Motion>",
			func=lambda e: self.setCell(*self.__coord2cell(e.x, e.y), "black"),
		)
		self.canvas.bind(
			"<B3-Motion>",
			func=lambda e: self.setCell(*self.__coord2cell(e.x, e.y), "white"),
		)
		# self.canvas.bind('<Button-1>', func=lambda e: print(f'Button1 pressed at {e.x}x{e.y}'))
		# self.canvas.bind('<Button-1>', func=lambda e: print(f'Button1 pressed at cell {self.__coord2cell(e.x, e.y)}'), add='+')
		# self.canvas.bind('<Button-3>', func=lambda e: print(f'Button3 pressed at {e.x}x{e.y}'))
		# self.canvas.bind('<Button-3>', func=lambda e: print(f'Button3 pressed at cell {self.__coord2cell(e.x, e.y)}'), add='+')

	def setCell(self, x: int, y: int, color: str) -> None:
		self.canvas.itemconfigure(self.cells[y][x], fill=color)
	
	def setAll(self, grid: list[list[int]]):
		assert(len(grid) == self.height//self.pixel_h)
		assert(len(grid[0]) == self.width//self.pixel_w)
		for y in range(len(grid)):
			for x in range(len(grid[y])):
				self.setCell(x, y, color='black' if grid[y][x] == 1 else 'white')

	def getGrid(self):
		return [
			[
				self.canvas.itemcget(self.cells[y][x], "fill")
				for x in range(len(self.cells[y]))
			]
			for y in range(len(self.cells))
		]

	def __coord2cell(self, x: int, y: int) -> tuple[int, int]:
		return (x // self.pixel_w, y // self.pixel_h)

	def __drawGridLines(self) -> None:
		for x in range(0, self.width, self.pixel_w):
			self.canvas.create_line((x, 0), (x, self.height), fill="black")
		for y in range(0, self.height, self.pixel_h):
			self.canvas.create_line((0, y), (self.width, y), fill="black")

	def updateGrid(self) -> None:
		# self.after(16, func=lambda: self.updateGrid())
		pass


def main() -> None:
	window = tk.Tk()
	grid = Grid(master=window)
	window.mainloop()


if __name__ == "__main__":
	main()
